import os
from dataclasses import dataclass
from pathlib import Path

from loguru import logger

from . import config


@dataclass
class Reading:
    """A formatted reading"""

    timestamp: int
    temp: int
    pressure: int
    accel: int

    def __repr__(self) -> str:
        return f"lettura di {self.timestamp}"


class FileProcessor:
    """An object that extracts readings from a file"""

    single_reading_bytearray_lenght = 16

    def __init__(self, filepath: Path):
        self.readings_file = filepath
        self.full_file_as_bytearray: bytearray = bytearray()
        self.readings_barrays_list: list[bytearray] = []
        self.readings: list[Reading] = []

    def __repr__(self) -> str:
        return f"Processor per il file: {self.readings_file}"

    def extract_readings(self) -> None:
        """Extract readings from the original data file"""
        self._get_bytearray_from_file()
        self._split_bytearray_into_readings_bytearrays()
        for barray in self.readings_barrays_list:
            self.readings.append(self._get_reading_from_bytearray(barray))
        logger.info(f"extracted data for file {self.readings_file.name}")

    def _get_bytearray_from_file(self) -> bytearray:
        """Trasform file into a bytearray"""
        with open(self.readings_file, "rb") as the_file:
            data = the_file.read()
            self.full_file_as_bytearray = bytearray(data)
            return self.full_file_as_bytearray

    def _split_bytearray_into_readings_bytearrays(self) -> list[bytearray]:
        """Get a list of readings bytearray from the bytearray
        rapresenting the whole file"""
        self.readings_barrays_list = []
        start_index = self.full_file_as_bytearray.index(b"\xca\xcb\xcc")
        for x in range(
            start_index,
            len(self.full_file_as_bytearray),
            FileProcessor.single_reading_bytearray_lenght,
        ):
            reading_chunk = self.full_file_as_bytearray[
                x : FileProcessor.single_reading_bytearray_lenght + x
            ]
            if len(reading_chunk) < 16:
                reading_chunk = reading_chunk + bytearray(
                    FileProcessor.single_reading_bytearray_lenght - len(reading_chunk)
                )
            self.readings_barrays_list.append(reading_chunk)
        self.readings_barrays_list.pop()
        return self.readings_barrays_list

    def _get_reading_from_bytearray(self, reading_bytearray: bytearray) -> Reading:
        """Create Reading instance from a bytearray"""
        ba_epoch = reading_bytearray[4:8]
        epoch = int.from_bytes(ba_epoch, "little")
        ba_temperature = reading_bytearray[8:10]
        temperature = int.from_bytes(ba_temperature, "little")
        ba_pressure = reading_bytearray[10:12]
        pressure = int.from_bytes(ba_pressure, "little")
        ba_accel = reading_bytearray[12:14]
        accel = int.from_bytes(ba_accel, "little")
        return Reading(epoch, temperature, pressure, accel)

    @staticmethod
    def is_file_valid(file: Path) -> bool:
        if os.path.getsize(file) >= 36:
            return True
        else:
            return False

    @staticmethod
    def get_purged_list(list_to_purge: list[Path], purge_thold: int = 36) -> list[Path]:
        purged_list = []
        for element in list_to_purge:
            if element.stat().st_size <= purge_thold:
                element.rename(config.DISCARDED_DIR / element.name)
            else:
                purged_list.append(element)
        return purged_list

    @staticmethod
    def get_files_list() -> list[Path]:
        """Generate a list of files to be processed"""
        file_list = [
            child for child in Path(config.READINGS_DIR).iterdir() if child.is_file()
        ]
        return file_list
