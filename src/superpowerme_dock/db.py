import os

import peewee as pw


sqlite_db = pw.SqliteDatabase(f'{os.environ.get("PREFIX")}_db')


class BaseModel(pw.Model):
    class Meta:
        database = sqlite_db


class Reading(BaseModel):
    id = pw.AutoField()
    reading_date = pw.DateTimeField()
    temp = pw.IntegerField()
    press = pw.IntegerField()
    accel = pw.IntegerField()
    created_at = pw.DateTimeField()
    uploaded_at = pw.DateTimeField()
    temp_thold = pw.IntegerField()
    press_thold = pw.IntegerField()
    accel_thold = pw.IntegerField()
