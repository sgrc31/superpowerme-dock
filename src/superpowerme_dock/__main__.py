import os
from pathlib import Path

from loguru import logger

from . import config
from .cli import app


def main():
    config.check_resource(config.DATA_DIR)
    config.check_resource(config.READINGS_DIR)
    config.check_resource(config.PROCESSED_READINGS_DIR)
    config.check_resource(config.DISCARDED_DIR)
    print("da settings")
    print(config.BASE_DIR)
    print(config.DATA_DIR)
    print(config.READINGS_DIR)
    print(config.PROCESSED_READINGS_DIR)
    app()


if __name__ == "__main__":
    main()
