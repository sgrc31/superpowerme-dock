#!/usr/bin/env python3
import typer
from loguru import logger

from .processor import FileProcessor


app = typer.Typer()


@app.command()
def upload_data():
    print("primo comando in cli")


@app.command()
def process_data() -> None:
    new_files_list = FileProcessor.get_files_list()
    if not new_files_list:
        logger.info("No new files to process, exiting...")
        return
    valid_files_list = FileProcessor.get_purged_list(new_files_list)
    logger.info(f"found n. {len(valid_files_list)} data files")
    processors_list = []
    for x in valid_files_list:
        processor = FileProcessor(x)
        processor.extract_readings()
        processors_list.append(processor)


@app.command()
def read_data():
    print("terzo comando in cli")
