import os
from pathlib import Path

from loguru import logger
from pydantic import BaseSettings
from pydantic import Field
from pydantic import validator


# We set paths
BASE_DIR = Path(__file__).resolve().parent.parent.parent

DATA_DIR = BASE_DIR / "data"

READINGS_DIR = DATA_DIR / "readings"

PROCESSED_READINGS_DIR = READINGS_DIR / "processed"

DISCARDED_DIR = READINGS_DIR / "discarded"


def check_resource(resource: Path) -> None:
    if not Path(resource).exists():
        logger.info(f"{resource.name} directory not found")
        raise FileNotFoundError(f"{resource.name} directory not found")


class Settings(BaseSettings):
    api_endpoint: str = Field(..., env="API_ENDPOINT")
    api_pass: str = Field(..., env="API_PASS")
    patient_prefix: str = Field(..., env="PATIENT_PREFIX")
    temp_thold: int = Field(..., env="TEMP_THRESHOLD")
    press_thold: int = Field(..., env="PRESS_THRESHOLD")
    accel_thold: int = Field(..., env="ACCEL_THRESHOLD")
    db_id: int = Field(..., env="DB_ID")

    @validator("temp_thold", "accel_thold", "press_thold", allow_reuse=True)
    def value_out_of_range(cls, v, field, values, **kwargs):
        if v not in range(0, 4096):
            raise ValueError(
                f"{field.name} value must be in between 0 and 4096 (current: {v})"
            )
        return v

    @validator("patient_prefix")
    def check_prefix_lenght(cls, v):
        if len(v) != 5:
            raise ValueError("Wrong patient prefix length, should be 5 letters long")

    class Config:
        env_file = ".env"
